#!/usr/bin/make -f

GROUPS = lflank rflank tusks hide head belly

PAGES = $(foreach g,$(GROUPS),$(g).pdf)
PHOTOS = $(wildcard *.jpg)
RESIZED_PHOTOS = $(foreach p,$(PHOTOS),imgs/$(p))

woolly.pdf: instructions.pdf diagram.pdf
	pdftk $^ cat output $@

%.pdf: woolly.svg
	inkscape --export-pdf $@ --export-id=g-$* $<

diagram.pdf: $(PAGES)
	pdftk $^ cat output $@

instructions.pdf: instructions.txt
	pandoc $< -o $@

index.html: instructions.txt header.html footer.html 
	(cat header.html && markdown $<  && cat footer.html) > $@

woolly.png: woolly.svg
	inkscape $< --export-png=$@ --export-area-drawing --export-background=white --export-dpi=13

imgs/%.jpg: %.jpg
	mkdir -p imgs
	gm convert $< -resize 320 $@

publish: index.html $(RESIZED_PHOTOS) woolly.pdf woolly.svg instructions.txt style.css furbg.png woolly.png
	rsync $^ che.mayfirst.org:public_html/woolly/

clean:
	rm -f $(PAGES) x woolly.pdf index.html $(RESIZED_PHOTOS) woolly.png instructions.pdf diagram.pdf

.PHONY: clean publish
