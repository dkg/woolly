Woolly Mammoth Stuffed Animal Project
=====================================

These are the sources for the plans for making a woolly mammoth
stuffed animal.

If you're just making the beast, you probably want to [see the
rendered plans](https://dkg.fifthhorseman.net/woolly/).

Enjoy!
